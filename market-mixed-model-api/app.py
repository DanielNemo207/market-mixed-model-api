#!flask/bin/python
import numpy as np
import pandas as pd
from flask import Flask, request, jsonify
import pickle
from Linear_regression import linear_regression_model
from data_handler import data_handler

app = Flask(__name__)
filename = 'Linear_regression_model.sav'
model = linear_regression_model()

@app.route('/')
def index():     
    return "test api"

@app.route("/api/predict", methods=['POST'])
def get_prediction(): 
    data = request.get_json(force=True) 
    data_df = pd.DataFrame.from_dict(data['up coming weeks data']).T
    data_df.index = range(len(data_df))    

    handler = data_handler()
    cols = np.array(['internal coef', 'temp', 'tv_adstock', 'rad_adstock', 'dig_adstock'])
    #handled_data = np.array([cols])
    current_sales = data['current sales']
    current_temp = data_df.loc[1, 'temp']

    current_data = handler.rebuild_current_data(current_sales, current_temp)
    next_week_data = handler.estimate_next_week_data(current_data, data_df.loc[0,:])
    handled_data = np.array([next_week_data])

    for i in range(1,len(data_df)):
        week_data = handler.estimate_next_week_data(handled_data[i-1], data_df.loc[i,:])
        handled_data = np.append(handled_data, np.array([week_data]), axis = 0)

    handled_data_df = pd.DataFrame(data=handled_data, columns = cols)

    predictions = pd.DataFrame(data = model.get_prediction(handled_data_df))
    predictions.columns = ['Predictions of sale']
    predictions.index = range(1, len(predictions) + 1)
    return predictions.to_json()

@app.route("/api/update", methods=['POST'])
def update_model():
    data = request.get_json(force=True)
    pickle.dump(data, open(filename, 'wb'))

if __name__ == '__main__':
    app.run(port=5000, debug=True)
	

