import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split

class data_handler:
    __model_param_filename = 'model_param.pickle'
    __tv_param_filename = 'tv_param.pickle'
    __rad_param_filename = 'rad_param.pickle'
    __dig_param_filename = 'dig_param.pickle'

    def __init__(self):
        self.load_tv_param()
        self.load_rad_param()
        self.load_dig_param()
        self.load_model_param()

    def load_tv_param(self):
        #params = pickle.loads(open(self.__tv_param_filename)) 
        with open(self.__tv_param_filename, 'rb') as handle:
            params = pickle.load(handle)
        self.__tv_boost = params['boost']
        self.__tv_decay = params['decay']
        self.__tv_dimi = params['diminishing']

    def load_rad_param(self):
        #params = pickle.loads(open(self.__rad_param_filename)) 
        with open(self.__rad_param_filename, 'rb') as handle:
            params = pickle.load(handle)
        self.__rad_boost = params['boost']
        self.__rad_decay = params['decay']
        self.__rad_dimi = params['diminishing']

    def load_dig_param(self):
        #params = pickle.loads(open(self.__dig_param_filename)) 
        with open(self.__dig_param_filename, 'rb') as handle:
            params = pickle.load(handle)
        self.__dig_boost = params['boost']
        self.__dig_decay = params['decay']
        self.__dig_dimi = params['diminishing']
        
    def load_model_param(self):
        #params = pickle.loads(open(self.__model_param_filename)) 
        with open(self.__model_param_filename, 'rb') as handle:
            params = pickle.load(handle)
        self.__base_value = params['base']
        self.__organic_growth_rate = params['organic']
        self.__temp_param = params['temp']

    def estimate_adstocks(self, sales):
        return (self.__tv_boost * sales, self.__rad_boost * sales, self.__dig_boost * sales)

    def rebuild_current_data(self, curent_sales, current_temp):
        current_tv_adstock, current_rad_adstock, current_dig_adstock = self.estimate_adstocks(curent_sales)
        current_internal_value = curent_sales - current_tv_adstock - current_rad_adstock - current_dig_adstock - self.__temp_param * current_temp
        return np.array([current_internal_value, current_tv_adstock, current_rad_adstock, current_dig_adstock])

    def cal_decay_val(self, A, rate):
        d = float(rate) * float(A)
        return float(d)

    def cal_adstock(self, T, v, d): 
        A = (1.0 / (1.0 + np.exp(float(-T / v)))) * float(T) + float(d)
        return float(A)

    def estimate_next_week_data(self, current_data, next_data):
        current_internal_value = current_data[0]
        current_tv_adstock = current_data[1]
        current_rad_adstock = current_data[2]
        current_dig_adstock = current_data[3]
        next_internal_coef = (current_internal_value / self.__base_value) + self.__organic_growth_rate
        next_tv = self.cal_adstock(next_data['tv_grps'], self.__tv_dimi, self.cal_decay_val(current_tv_adstock, self.__tv_decay))
        next_rad = self.cal_adstock(next_data['radio_grps'], self.__rad_dimi, self.cal_decay_val(current_rad_adstock, self.__rad_decay))
        next_dig = self.cal_adstock(next_data['digital_grps'], self.__dig_dimi, self.cal_decay_val(current_dig_adstock, self.__dig_decay))
        return np.array([next_internal_coef, next_data['temp'], next_tv, next_rad, next_dig])





    