import pickle
import pandas as pd
import numpy as np

oraganic_growth_rate = 0.1

tv_decay = .775
rad_decay = .45
dig_decay = .75

tv_dimi = 135
rad_dimi = 165
dig_dimi = 85

tv_boost = 0.40637088886089767
rad_boost = 0.0689733805948911
dig_boost = 0.2566542867210826

tv = {
    'boost' : tv_boost, 'decay' : tv_decay, 'diminishing': tv_dimi
}
rad = {
    'boost' : rad_boost, 'decay' : rad_decay, 'diminishing': rad_dimi
}
dig = {
    'boost' : dig_boost, 'decay' : dig_decay, 'diminishing': dig_dimi
}
model = {
    'base' : 4.107498124125593, 'organic': oraganic_growth_rate, 'temp' : 2.22204132348816 
}

pickle.dump(model, open('model_param.pickle', 'wb'))
pickle.dump(tv, open('tv_param.pickle', 'wb'))
pickle.dump(rad, open('rad_param.pickle', 'wb'))
pickle.dump(dig, open('dig_param.pickle', 'wb'))