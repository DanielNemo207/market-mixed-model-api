import pickle
import pandas as pd
import numpy as np

class linear_regression_model:
    model_filename = 'Linear_regression_model.sav'    
    model = None
    
    def __init__(self):
        self.load_model()

    def load_model(self):
        loaded_model = pickle.load(open(self.model_filename, 'rb')) 
        self.model = loaded_model

    def get_prediction(self,input):
        predictions = self.model.predict(input)
        return predictions    